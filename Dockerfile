FROM openjdk:8
EXPOSE 8080
ADD target/sb-redis-docker.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]