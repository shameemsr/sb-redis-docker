### About

This sample **Spring Boot** project shows how to use **Caching** with **Redis** using `@EnableCaching` annotation. **Redis** is an open source, in-memory data structure store, which can be used as a database, cache and message broker. It supports various data structures such as strings, hashes, lists, sets, sorted sets with range queries and many others.

This application can be deployed as **Docker Compose** along with a Redis containerized service.

---

### Code Example

`@EnableCaching` annotation is used on the application main class in order to enable caching.

**Application Main class** (`com.example.redis.SbRedisDockerApplication`): This main class itself is a REST controller, as annotated with `@RestController`. 

```java
@SpringBootApplication
@EnableCaching
@RestController
public class SbRedisDockerApplication {
	public static void main(String[] args) {
		SpringApplication.run(SbRedisDockerApplication.class, args);
	}
}
```

---

**Redis Config class** (`com.example.redis.config.RedisConfig`): A Spring *configuration* class for creating Redis connection factory.

```java
@Configuration
public class RedisConfig {
	
	@Value("${spring.redis.host}")
	private String host;
	@Value("${spring.redis.port}")
	private Integer port;
	
	@Bean
	JedisConnectionFactory getJedisConnectionFactory() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setPort(port);

        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration.build());
	} 

	@Bean 
	RedisTemplate<String, Object> getRedisTemplate() {
		final RedisTemplate<String, Object> template = ...
		return template;
	}
}
```

---

** Property file** (`application.yml`)

```yml
spring:
  redis:
    host: redis
    port: 6379
```

---

**Redis Operations** (`com.example.redis.repository.StudentService`): This service class performs get/set operations on Redis server. 

```java
@Service
public class StudentService ... {

	public void save(Student student) {
		hashOperations.put(TABLE_NAME, student.getId(), student);
	}

	public Student find(int id) {
		return hashOperations.get(TABLE_NAME, id);
	}
}
```

---

**Controller** (`com.example.redis.controller.RedisController`): This controller class provides REST API endpoints for performing Redis get/set operations.   

```java
@RestController
@RequestMapping("/redis")
public class RedisController {

	@Autowired
	private StudentService studentService;
	
	@PostMapping
	public ResponseEntity saveStudent(@RequestBody Student student) {
		studentService.save(student);
		return new ResponseEntity(CREATED);
	}
	
	@Cacheable(key = "#id", value = "students")
	@GetMapping(path = "{id}")
	public Student getStudentById(@PathVariable("id") int id) {
		return studentService.find(id);
	}
}
```

---

**Caching** `getStudentById(...)` method in the controller class is annotated by `@Cacheable` which makes the value returned by the `getStudentById(...)` method to be cached after the first call with a given *id*. Any subsequent call will return the value from Redis cache for the same *id*.

For example, lets make a **POST** request to `/redis` with the following request body (payload):

```json
{
  "name": "Abc",
  "id": 101
}
```

This results in a **HSET** in Redis server:

```
[0 127.0.0.1:62407] "HSET" "\xac\xed\x00\x05t\x00\aStudent" "\xac\xed\x00\x05sr\x00\x11java.lang.Integer\x12\xe2\xa0\xa4\xf7\x81\x878\x02\x00\x01I\x00\x05valuexr\x00\x10java.lang.Number\x86\xac\x95\x1d\x0b\x94\xe0\x8b\x02\x00\x00xp\x00\x00\x00e" "\xac\xed\x00\x05sr\x00\x1fcom.example.redis.model.Student\x00\x00\x00\x00\x00\x00\x00\x01\x02\x00\x02I\x00\x02idL\x00\x04namet\x00\x12Ljava/lang/String;xp\x00\x00\x00et\x00\x03Abc"
```

Whenever the first attempt is made to retrieve this value via **GET** request to `/redis/101`, **HGET** is performed on the Redis server:

```
[0 127.0.0.1:62416] "HGET" "\xac\xed\x00\x05t\x00\aStudent" "\xac\xed\x00\x05sr\x00\x11java.lang.Integer\x12\xe2\xa0\xa4\xf7\x81\x878\x02\x00\x01I\x00\x05valuexr\x00\x10java.lang.Number\x86\xac\x95\x1d\x0b\x94\xe0\x8b\x02\x00\x00xp\x00\x00\x00e"
```

Since `@Cacheable` is used, the result is cached via **SET**:

```
[0 127.0.0.1:62417] "SET" "students::101" "\xac\xed\x00\x05sr\x00\x1fcom.example.redis.model.Student\x00\x00\x00\x00\x00\x00\x00\x01\x02\x00\x02I\x00\x02idL\x00\x04namet\x00\x12Ljava/lang/String;xp\x00\x00\x00et\x00\x03Abc"
```

Any subsequent REST API **GET** request would return the value from cache via **GET** operation (unlike the first request when the cache is set): 

```
[0 127.0.0.1:62421] "GET" "students::101"
```


---

And that's how caching can be done with Redis!! 

---

**docker-compose** for running with Redis container.

```
version: '3'
services:
  web:
    image: shameemsr/sb-redis-docker:v1
    ports:
     - "8080:8080"
  redis:
    image: redis
    command: [ "redis-server", "--protected-mode", "no" ]
    ports:
      - "6379:6379"
```

---

**Dockerfile** for creating Docker image of this application.

```
FROM openjdk:8
EXPOSE 8080
ADD target/sb-redis-docker.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
```

Docker Pull Command: `docker pull shameemsr/sb-redis-docker:v1`

---

**Maven** dependencies

```xml

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>
		
		<dependency>
			<groupId>redis.clients</groupId>
			<artifactId>jedis</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
```

---

### Reference Documentation
For further reference, please consider the following sections:


* [Messaging with Redis](https://spring.io/guides/gs/messaging-redis/)
* [Redis](https://redis.io/)

