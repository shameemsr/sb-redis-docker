package com.example.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableCaching
@RestController
public class SbRedisDockerApplication {
	
	private static final Logger log = LoggerFactory.getLogger(SbRedisDockerApplication.class);
	
	@Value("${spring.application.name}")
	private String springAppName;

	public static void main(String[] args) {
		SpringApplication.run(SbRedisDockerApplication.class, args);
		log.info("Web application started...");
	}

	@RequestMapping("/")
	public String greeting() {
		return springAppName + " is running.";
	}

}
