package com.example.redis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.HttpStatus.CREATED;

import com.example.redis.model.Student;
import com.example.redis.repository.StudentService;

@RestController
@RequestMapping("/redis")
public class RedisController {

	@Autowired
	private StudentService studentService;
	
	@PostMapping
	public ResponseEntity saveStudent(@RequestBody Student student) {
		studentService.save(student);
		return new ResponseEntity(CREATED);
	}
	
	@Cacheable(key = "#id", value = "students")
	@GetMapping(path = "{id}")
	public Student getStudentById(@PathVariable("id") int id) {
		return studentService.find(id);
	}
	
	@GetMapping(path = "/all")
	public List<Student> getAllStudents() {
		return studentService.findAll();
	}
}
