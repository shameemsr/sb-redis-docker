package com.example.redis.repository;

import java.util.List;

import com.example.redis.model.Student;

public interface StudentRepository {

	void save(Student student);
	
	Student find(int id);
	
	List<Student> findAll();
}
