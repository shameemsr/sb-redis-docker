package com.example.redis.repository;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.example.redis.model.Student;

@Service
public class StudentService implements StudentRepository {
	
	private static final String TABLE_NAME = "Student";
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	private HashOperations<String, Integer, Student> hashOperations;
	
	@PostConstruct
	private void initializeHashOperations() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(Student student) {
		hashOperations.put(TABLE_NAME, student.getId(), student);
	}

	@Override
	public Student find(int id) {
		return hashOperations.get(TABLE_NAME, id);
	}

	@Override
	public List<Student> findAll() {
		return hashOperations.values(TABLE_NAME);
	}

}
